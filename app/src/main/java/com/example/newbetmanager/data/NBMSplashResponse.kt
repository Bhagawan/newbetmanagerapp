package com.example.newbetmanager.data

import androidx.annotation.Keep

@Keep
class NBMSplashResponse(val url : String)