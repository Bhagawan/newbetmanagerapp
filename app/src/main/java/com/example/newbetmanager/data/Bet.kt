package com.example.newbetmanager.data

import androidx.annotation.Keep

@Keep
data class Bet(val amount: Int, val coefficient: Float, var state: Boolean?) {

    override fun equals(other: Any?): Boolean {
        return if(other is Bet) amount == other.amount && coefficient == other.coefficient
        else false
    }

    override fun hashCode(): Int {
        var result = amount
        result = 31 * result + coefficient.hashCode()
        result = 31 * result + (state?.hashCode() ?: 0)
        return result
    }
}