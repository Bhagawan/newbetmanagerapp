package com.example.newbetmanager.util

import com.example.newbetmanager.data.NBMSplashResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ServerClientNewBetManager {

    @FormUrlEncoded
    @POST("NewBetManagerApp/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<NBMSplashResponse>

    companion object {
        fun create() : ServerClientNewBetManager {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClientNewBetManager::class.java)
        }
    }

}