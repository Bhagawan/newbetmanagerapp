package com.example.newbetmanager.util

import android.content.Context
import com.example.newbetmanager.data.Bet
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SavedPrefsNBM {
    companion object {
        fun saveBet(context: Context, bet: Bet) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            val g = Gson()
            val oldBets = g.fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList<Bet>()
            var updating = false
            for(b in oldBets) {
                if(b == bet) {
                    updating = true
                    b.state = bet.state
                }
            }
            val newBets = if(!updating) oldBets.plus(bet) else oldBets
            shP.edit().putString("bets", g.toJson(newBets)).apply()
        }

        fun getBets(context: Context) : List<Bet> {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            return Gson().fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList()
        }

        fun removeBet(context: Context, bet: Bet) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            val g = Gson()
            val savedBets = g.fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList<Bet>()
            val changedBets = savedBets.minus(bet)
            shP.edit().putString("bets", g.toJson(changedBets)).apply()
        }

        fun saveMoney(context: Context, amount: Int) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            shP.edit().putInt("cash", amount).apply()
        }

        fun getMoney(context: Context) : Int {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            return shP.getInt("cash", 0)
        }
    }
}