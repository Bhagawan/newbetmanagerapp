package com.example.newbetmanager.viewModel

import android.os.Build
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import com.example.newbetmanager.BR
import com.example.newbetmanager.util.ServerClientNewBetManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import java.util.*

class SplashViewModelNBM(simLanguage: String): BaseObservable() {
    private val splashScope = CoroutineScope(Dispatchers.IO)
    private var request: Job

    @Bindable
    var url  = ""

    @Bindable
    var logoVisible = true

    val logoUrl = "http://195.201.125.8/NewBetManagerApp/logo.png"

    val switchToMain = MutableLiveData<Boolean>()

    init {
        request = splashScope.async {
            val splash = ServerClientNewBetManager.create().getSplash(
                Locale.getDefault().language,
                simLanguage,
                Build.MODEL,
                TimeZone.getDefault().displayName.replace("GMT", "")
            )
            if (splash.isSuccessful) {
                logoVisible = false
                notifyPropertyChanged(BR.logoVisible)

                if (splash.body() != null) {
                    if (splash.body()!!.url == "no") switchToMain.postValue(true)
                    else {
                        url = "https://${splash.body()!!.url}"
                        notifyPropertyChanged(BR.url)
                    }
                } else switchToMain.postValue(true)
            } else switchToMain.postValue(true)
        }
    }

    fun destroy() {
        if(request.isActive) request.cancel()
    }

}