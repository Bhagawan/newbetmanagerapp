package com.example.newbetmanager.viewModel

import android.content.Context
import android.graphics.Color
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.newbetmanager.BR
import com.example.newbetmanager.data.Bet
import com.example.newbetmanager.util.SavedPrefsNBM

class BetViewModel(private var bet: Bet): BaseObservable() {
    val amount = bet.amount.toString()
    val coefficient = bet.coefficient.toString()
    private var betInterface: BetInterface? = null

    @get:Bindable
    val stateText: String
    get() = when (bet.state) {
        true -> "Успех"
        false -> "Неудача"
        else -> "В ожидании"
    }

    @get:Bindable
    val stateColor :Int
    get() = when (bet.state) {
        true -> Color.GREEN
        false -> Color.RED
        else -> Color.GRAY
    }

    @Bindable
    var buttonsVisible = false

    companion object {
        const val WIN = 0
        const val WAITING = 1
        const val LOSE = 2
    }

    fun setInterface(i: BetInterface) {
        this.betInterface = i
    }

    fun changeState(context: Context) = changeState(context, null)

    fun changeState(context: Context, state: Int?) {
        when(state) {
            WIN -> {
                if(bet.state != true) betInterface?.onMoneyChanges((bet.amount * bet.coefficient).toInt())
                bet.state = true
            }
            WAITING -> {
                if(bet.state == true) betInterface?.onMoneyChanges(-(bet.amount * bet.coefficient).toInt())
                bet.state = null
            }
            LOSE -> {
                if(bet.state == true) betInterface?.onMoneyChanges(-(bet.amount * bet.coefficient).toInt())
                bet.state = false
            }
        }
        state?.let { SavedPrefsNBM.saveBet(context, bet) }
        buttonsVisible = ! buttonsVisible
        notifyPropertyChanged(BR.stateText)
        notifyPropertyChanged(BR.stateColor)
        notifyPropertyChanged(BR.buttonsVisible)
    }

    fun deleteBet(context: Context) {
        SavedPrefsNBM.removeBet(context, bet)
        betInterface?.deleteBet()
    }

    interface BetInterface {
        fun deleteBet()
        fun onMoneyChanges(amount: Int)
    }
}