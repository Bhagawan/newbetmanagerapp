package com.example.newbetmanager.viewModel

import androidx.lifecycle.MutableLiveData

class AddMoneyViewModel {
    var amount = ""

    val exit = MutableLiveData<Int>()
    val error = MutableLiveData<String>()

    fun finish(amount: Int) = exit.postValue(amount)

    fun finish() {
        try {
            val m = amount.toInt()
            exit.postValue(m)
        } catch (e : Exception) {
            error.postValue("Ввведите целое число")
        }
    }
}