package com.example.newbetmanager.viewModel

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.example.newbetmanager.data.Bet
import com.example.newbetmanager.util.SavedPrefsNBM

class NewBetViewModel: BaseObservable() {
    var amount = ""
    var coefficient = ""

    val close = MutableLiveData<Boolean>()
    val toast = MutableLiveData<String>()

    fun addBet(context: Context) {
        try {
            val a = amount.toInt()
            var current = SavedPrefsNBM.getMoney(context)
            if(current < a) throw Exception("broke")
            val coeff = coefficient.toFloat()
            SavedPrefsNBM.saveBet(context, Bet(a,coeff, null))
            current -= a
            SavedPrefsNBM.saveMoney(context, current)
            close.postValue(true)

        } catch (e: Exception) {
            if(e.message == "broke") toast.postValue("Недостаточно средств на счете")
            else toast.postValue("Поля не должны содержать посторонних символов")
        }
    }

    fun cancel() {
        close.postValue(false)
    }
}