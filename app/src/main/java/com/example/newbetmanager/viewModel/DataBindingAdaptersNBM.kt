package com.example.newbetmanager.viewModel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView

@BindingAdapter("imageUrl")
fun loadUrl(view: ImageView, url: String) {
     Picasso.get().load(url).into(view)
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}
