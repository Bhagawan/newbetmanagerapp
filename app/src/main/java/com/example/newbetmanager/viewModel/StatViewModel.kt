package com.example.newbetmanager.viewModel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.newbetmanager.data.Bet

class StatViewModel: BaseObservable() {
    @Bindable
    var totalAmount = ""
    @Bindable
    var success = ""
    @Bindable
    var successPercent = ""
    @Bindable
    var betAmount = ""
    @Bindable
    var roi = ""
    @Bindable
    var roc = ""
    @Bindable
    var averageBet = ""
    @Bindable
    var averageCoefficient = ""

    fun updateData(betDatabase: List<Bet>, cash: Int) {
        var total = cash
        var successful = 0.0f
        val amount = betDatabase.size
        var successfulBets = 0
        var betSumm = 0
        var coeffSumm = 0.0f

        for(bet in betDatabase) {
            total += bet.amount
            successful += (bet.amount * bet.coefficient * (if (bet.state == true) 1  else 0))
            if(bet.state == true) successfulBets ++
            betSumm += bet.amount
            coeffSumm += bet.coefficient
        }
        totalAmount = total.toString()
        success = successful.toString()
        successPercent = "${if(amount * successfulBets == 0) 100 else 100 / amount * successfulBets} %"
        betAmount = amount.toString()
        roi = if(betSumm == 0) "0" else (successful / betSumm).toString()
        roc = (successful - betSumm).toString()
        averageBet = if(amount > 0) (betSumm / amount).toString() else "0"
        averageCoefficient = if(amount > 0) (coeffSumm / amount).toString() else "0"

        notifyChange()
    }
}