package com.example.newbetmanager.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.newbetmanager.databinding.FragmentStatBinding
import com.example.newbetmanager.util.SavedPrefsNBM
import com.example.newbetmanager.viewModel.StatViewModel

class StatFragment : Fragment() {
    private lateinit var binding: FragmentStatBinding
    private val viewModel = StatViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStatBinding.inflate(layoutInflater)
        binding.viewModel = viewModel

        return binding.root
    }

    fun updateData() {
        context?.let {viewModel.updateData( SavedPrefsNBM.getBets(it), SavedPrefsNBM.getMoney(it))}
    }

    override fun onResume() {
        super.onResume()
        context?.let {viewModel.updateData( SavedPrefsNBM.getBets(it), SavedPrefsNBM.getMoney(it))}
    }
}