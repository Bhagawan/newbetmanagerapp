package com.example.newbetmanager

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.customview.widget.ViewDragHelper
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newbetmanager.adapter.BetAdapter
import com.example.newbetmanager.databinding.ActivityMainBinding
import com.example.newbetmanager.databinding.PopupAddMoneyBinding
import com.example.newbetmanager.databinding.PopupNewBetBinding
import com.example.newbetmanager.fragment.StatFragment
import com.example.newbetmanager.util.SavedPrefsNBM
import com.example.newbetmanager.viewModel.AddMoneyViewModel
import com.example.newbetmanager.viewModel.NewBetViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loadBackground()

        binding.recyclerMain.layoutManager = LinearLayoutManager(this)
        binding.recyclerMain.adapter = BetAdapter().apply { setInterface(object : BetAdapter.BetAdapterInterface {
            override fun onMoneyChanges(amount: Int) {
                addToAccount(amount)
            }
        }) }

        binding.drawerMain.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerOpened(drawerView: View) { }
            override fun onDrawerClosed(drawerView: View) {}
            override fun onDrawerStateChanged(newState: Int) {
                if(newState == ViewDragHelper.STATE_DRAGGING || newState == ViewDragHelper.STATE_SETTLING) (supportFragmentManager.fragments[0] as StatFragment).updateData()
            }
        })

        updateBets()
        setSupportActionBar(binding.toolbar)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_baseline_query_stats_24)
        }

        binding.fabAddBet.setOnClickListener { createNewBet() }
        updateMoney()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.bet_action_bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> if(binding.drawerMain.isOpen) binding.drawerMain.close() else binding.drawerMain.open()
            R.id.btn_money_add -> addMoney()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun createNewBet() {
        val popupBinding: PopupNewBetBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.popup_new_bet, null, false)

//        val width = LinearLayout.LayoutParams.WRAP_CONTENT
//        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val width = popupBinding.root.minimumWidth
        val height = popupBinding.root.minimumHeight

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        val popupViewModel = NewBetViewModel()
        popupViewModel.close.observe(this) {
            if(it) updateBets()
            updateMoney()
            popupWindow.dismiss()
        }
        popupViewModel.toast.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupBinding.setVariable(BR.viewModel, popupViewModel)

        popupWindow.animationStyle = R.style.PopupBetAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addMoney() {
        val popupBinding: PopupAddMoneyBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.popup_add_money, null, false)

        val width = popupBinding.root.minimumWidth
        val height = popupBinding.root.minimumHeight

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        val popupViewModel = AddMoneyViewModel()
        popupViewModel.exit.observe(this) {
            if(it > 0) addToAccount(it)
            popupWindow.dismiss()
        }
        popupViewModel.error.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupBinding.setVariable(BR.viewModel, popupViewModel)

        popupWindow.animationStyle = R.style.PopupBetAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun updateBets() {
        (binding.recyclerMain.adapter as BetAdapter).updateList(SavedPrefsNBM.getBets(this))
    }

    private fun addToAccount(money: Int) {
        var current = SavedPrefsNBM.getMoney(this)
        current += money
        SavedPrefsNBM.saveMoney(this, current)
        updateMoney()
    }

    private fun updateMoney() {
        binding.textToolbarTitle.text = getString(R.string.string_money, SavedPrefsNBM.getMoney(this))
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load("http://195.201.125.8/NewBetManagerApp/back.png").into(target)
    }
}