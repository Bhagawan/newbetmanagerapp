package com.example.newbetmanager.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.newbetmanager.BR
import com.example.newbetmanager.R
import com.example.newbetmanager.data.Bet
import com.example.newbetmanager.viewModel.BetViewModel

class BetAdapter  : RecyclerView.Adapter<BetAdapter.ViewHolder>(), DynamicAdapter<List<Bet>> {
    private var items = ArrayList<Bet>()
    private var adapterInterface: BetAdapterInterface? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_bet, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<Bet>) {
        if (items.isEmpty()) {
            items.addAll(data)
            notifyDataSetChanged()
        }
    }

    private fun addItem(item: Bet) {
        items.add(item)
        notifyItemInserted(items.size)
    }

    fun updateList(bets: List<Bet>) {
        for (bet in bets) if(!items.contains(bet)) addItem(bet)
    }



    fun setInterface(adapterInterface: BetAdapterInterface) {
        this.adapterInterface = adapterInterface
    }

    inner class ViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Bet) {
            val vModel =BetViewModel(item)
            vModel.setInterface(object : BetViewModel.BetInterface {
                override fun deleteBet() {
                    items.remove(item)
                    notifyItemRemoved(adapterPosition)
                }

                override fun onMoneyChanges(amount: Int) {
                    adapterInterface?.onMoneyChanges(amount)
                }
            })
            binding.setVariable(BR.viewModel, vModel)
        }
    }

    interface BetAdapterInterface {
        fun onMoneyChanges(amount : Int)
    }
}