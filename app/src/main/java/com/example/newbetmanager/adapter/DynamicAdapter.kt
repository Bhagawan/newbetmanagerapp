package com.example.newbetmanager.adapter

interface DynamicAdapter<T> {
    fun setData(data: T)
}